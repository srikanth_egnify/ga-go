package main

import (
	"container/heap"
	"context"
	"fmt"
	"log"
	"strconv"
	"strings"
	"sync"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"

	"errors"
	"reflect"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/bsoncodec"
	"go.mongodb.org/mongo-driver/bson/bsonrw"
	"go.mongodb.org/mongo-driver/bson/bsontype"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const (
	settingsDatabase = "ui-china-settings-qa"
	analysisDatabase = "ui-china-test-management-qa"
)

var (
	mapMutex = sync.RWMutex{}
)

// InstituteHierarchy represents institutehierarchies db record
type InstituteHierarchy struct {
	ChildCode        string `bson:"childCode"`
	Child            string `bson:"child"`
	Level            int    `bson:"level"`
	IsLeafNode       bool   `bson:"isLeafNode"`
	Parent           string `bson:"parent"`
	ParentCode       string `bson:"parentCode"`
	Active           bool   `bson:"active"`
	numberOfStudents string `bson:"numberOfStudents"`
}

// CommonAnalysisStructure is a helper structure for topic, sub topic, subject analysis
type CommonAnalysisStructure struct {
	TotalMarks         int     `bson:"totalMarks"`
	TotalObtainedMarks int     `bson:"totalObtainedMarks"`
	TotalQuestion      int     `bson:"totalQuestion"`
	NumberOfQuestions  int     `bson:"numberOfQuestions"`
	AggregatedMarks    int     `bson:"aggregatedMarks"`
	NumberOfTests      int     `bson:"numberOfTests"`
	TimeSpent          int     `bson:"timespent"`
	MinMarks           int     `bson:"minMarks"`
	MaxMarks           int     `bson:"maxMarks"`
	Percentage         float64 `bson:"percentage"`
	C                  int     `bson:"C"`
	W                  int     `bson:"W"`
	U                  int     `bson:"U"`
	P                  int     `bson:"P"`
	ADD                int     `bson:"ADD"`
	UW                 int     `bson:"UW"`
}

// StudentSnapshot represents student snapshot in db
type StudentSnapshot struct {
	StudentID    string                       `bson:"studentId"`
	StudentName  string                       `bson:"studentName"`
	TestID       string                       `bson:"testId"`
	ResponseData map[string]map[string]string `bson:"responseData"`
	Hierarchy    []struct {
		ChildCode string `bson:"childCode"`
	} `bson:"hierarchy"`
}

// QuestionMapping represents questionmappings record in db
type QuestionMapping struct {
	Topic          string `bson:"topic"`
	SubTopic       string `bson:"subTopic"`
	Subject        string `bson:"subject"`
	QuestionNumber string `bson:"questionNumber"`
	C              int    `bson:"C"`
	W              int    `bson:"W"`
	U              int    `bson:"U"`
	ADD            int    `bson:"ADD"`
	P              int    `bson:"P"`
}

// Holy crap for schema less mongo docs
type nullawareDecoder struct {
	defDecoder bsoncodec.ValueDecoder
	zeroValue  reflect.Value
}

// StudentSubjectRank represents the subject wise rank analysis
type StudentSubjectRank struct {
	Rank          int     `bson:"rank"`
	ObtainedMarks int     `bson:"obtainedMarks"`
	Percentage    float64 `bson:"percentage"`
}

// StudentRankAnalysis represents rankanalysis record
type StudentRankAnalysis struct {
	StudentID    string                        `bson:"studentId"`
	Marks        int                           `bson:"marks"`
	Rank         int                           `bson:"rank"`
	Hierarchy    string                        `bson:"hierarchy"`
	SubjectRanks map[string]StudentSubjectRank `bson:"rankAnalysis"`
}

// HierarchyMarkAnalysis represents mark analysis for hierarchy
type HierarchyMarkAnalysis struct {
	TestID       string                    `bson:"testId"`
	Hierarchy    string                    `bson:"hierarchy"`
	MarkAnalysis map[string]map[string]int `bson:"markAnalysis"`
}

// HierarchicalAnalysis represents hierarchical analysis
type HierarchicalAnalysis struct {
	StudentTopicAnalysis `bson:"inline"`
	TestID               string                                `bson:"testId"`
	Hierarchy            string                                `bson:"hierarchy"`
	QuestionMap          map[string]map[string]QuestionMapping `bson:"questionMap"`
}

var hierarchicalAnalysis map[string]HierarchicalAnalysis

func (d *nullawareDecoder) DecodeValue(dctx bsoncodec.DecodeContext, vr bsonrw.ValueReader, val reflect.Value) error {
	if vr.Type() != bsontype.Null {
		return d.defDecoder.DecodeValue(dctx, vr, val)
	}

	if !val.CanSet() {
		return errors.New("value not settable")
	}
	if err := vr.ReadNull(); err != nil {
		return err
	}
	// Set the zero value of val's type:
	val.Set(d.zeroValue)
	return nil
}

func getCustomRegistryBuilder() *bsoncodec.RegistryBuilder {
	customValues := []interface{}{
		"",       // string
		int(0),   // int
		int32(0), // int32
	}
	rb := bson.NewRegistryBuilder()
	for _, v := range customValues {
		t := reflect.TypeOf(v)
		defDecoder, err := bson.DefaultRegistry.LookupDecoder(t)
		if err != nil {
			panic(err)
		}
		rb.RegisterDecoder(t, &nullawareDecoder{defDecoder, reflect.Zero(t)})
	}
	return rb
}

// getInstituteHierarchies builds institute's hierarchy map [code]Hierarchy and adjList
func getInstituteHierarchies(client *mongo.Client, hierarchyMap map[string]InstituteHierarchy, adjList map[string][]string, wg *sync.WaitGroup) {
	defer wg.Done()
	collection := client.Database(settingsDatabase).Collection("institutehierarchies")
	filter := bson.D{}

	cur, err := collection.Find(context.TODO(), filter)
	if err != nil {
		log.Fatal("Error while fetching institute hierarchies from DB")
		log.Fatal(err)
	}
	defer cur.Close(context.TODO())
	for cur.Next(context.TODO()) {
		// set result to empty InstituteHierarchy and decode into it.
		result := InstituteHierarchy{}
		err := cur.Decode(&result)
		if err != nil {
			log.Fatal(err)
		}
		hierarchyMap[result.ChildCode] = result
		if result.ParentCode != "" {
			adjList[result.ParentCode] = append(adjList[result.ParentCode], result.ChildCode)
		}
	}
}

// getStudentsSnapshot builds students test snapshot map[studentID]snapsot
func getStudentsSnapshot(client *mongo.Client, studentsSnapshotMap map[string]StudentSnapshot, wg *sync.WaitGroup) {
	defer wg.Done()
	testStudentSnapshot := client.Database(analysisDatabase).Collection("testStudentSnapshot")
	testFilter := bson.D{primitive.E{Key: "testId", Value: "000011"}}
	snapshotCursor, err := testStudentSnapshot.Find(context.TODO(), testFilter)
	if err != nil {
		log.Fatal("Error while fetching student's snapshot")
		log.Fatal(err)
	}
	defer snapshotCursor.Close(context.TODO())
	for snapshotCursor.Next(context.TODO()) {
		// create empty snapshot struct and decode into it
		studentSnapshot := StudentSnapshot{}
		err := snapshotCursor.Decode(&studentSnapshot)
		if err != nil {
			log.Fatal("Error while decoding student snapshot")
			log.Fatal(err)
		}
		studentsSnapshotMap[studentSnapshot.StudentID] = studentSnapshot
	}
}

// getQuestionMapping builds question mapping map[qustion]QuestionMappin
func getQuestionMapping(client *mongo.Client, questionMapping map[string]QuestionMapping, wg *sync.WaitGroup) {
	defer wg.Done()
	questionMappings := client.Database(analysisDatabase).Collection("questionmappings")
	testFilter := bson.D{primitive.E{Key: "testId", Value: "000011"}}
	cur, err := questionMappings.Find(context.TODO(), testFilter)
	if err != nil {
		log.Fatal("Error while fetching question mappings")
		log.Fatal(err)
	}
	defer cur.Close(context.TODO())
	for cur.Next(context.TODO()) {
		qMap := QuestionMapping{}
		err := cur.Decode(&qMap)
		if err != nil {
			log.Fatal("Error while decoding question mapping record")
			log.Fatal(err)
		}
		questionMapping[qMap.QuestionNumber] = qMap
	}
}

// calculateMasterResults computes the masterresults
func calculateMasterResults(args ...interface{}) {
	var wg sync.WaitGroup
	adjList := args[0].(map[string][]string)
	studentsSnapshotMap := args[1].(map[string]StudentSnapshot)
	questionMapping := args[2].(map[string]QuestionMapping)
	masterResultsMap := args[3].(map[string]*MasterResult)
	studentHierarchyMap := args[4].(map[string]*MasterResult)
	for studentID, studentSnapshot := range studentsSnapshotMap {
		wg.Add(1)
		go func(studentID string, studentSnapshot StudentSnapshot) {
			defer wg.Done()
			studentMasterResult := &MasterResult{}
			studentMasterResult.TestID = studentSnapshot.TestID
			studentMasterResult.StudentID = studentSnapshot.StudentID
			studentMasterResult.ID = strings.Join([]string{studentSnapshot.TestID, studentSnapshot.StudentID}, "_")
			studentQuestionResp := studentSnapshot.ResponseData["questionResponse"]
			studentMasterResult.StudentCWUAnalysis = make(map[string]CommonCWUStructure)
			studentMasterResult.Analysis = make(map[string]SubjectAnalysis)
			studentMasterResult.PopulateQuestionResponse(studentQuestionResp, questionMapping)
			studentMasterResult.PopulateCWUData(studentQuestionResp, questionMapping)
			studentMasterResult.PopulateAnalysis(studentQuestionResp, questionMapping)
			studentLastHierarchy := studentSnapshot.Hierarchy[0].ChildCode
			mapMutex.Lock()
			adjList[studentLastHierarchy] = append(adjList[studentLastHierarchy], studentID)
			masterResultsMap[studentID] = studentMasterResult
			studentHierarchyMap[studentLastHierarchy] = studentMasterResult
			mapMutex.Unlock()
		}(studentID, studentSnapshot)
	}
	wg.Wait()
}

// calculateHierarchyRanks computes the hierarchy wise ranks
func calculateHierarchyRanks(
	hierarchy string,
	studentMarksMap map[string]*MasterResult, adjList map[string][]string, hierarchyWiseRanks map[string][]StudentRankAnalysis) []StudentRankAnalysis {
	data := []StudentRankAnalysis{}
	childNodes, hasChildNodes := adjList[hierarchy]
	if hasChildNodes {

		childNodesData := []StudentRankAnalysis{}
		orderedChildNodesData := []StudentRankAnalysis{}
		for _, childNode := range childNodes {
			tempData := calculateHierarchyRanks(childNode, studentMarksMap, adjList, hierarchyWiseRanks)
			for _, item := range tempData {
				childNodesData = append(childNodesData, item)
			}
		} // end
		subjects := []string{"Physics", "Chemistry", "Maths", "overall"}
		// subjects wise ranks
		studentSubjectWiseRanks := make(map[string]map[string]int)
		for _, subject := range subjects {
			pq := make(PriorityQueue, 0)
			heap.Init(&pq)
			index := 0
			for _, item := range childNodesData {
				Marks := item.Marks
				if subject != "overall" {
					Marks = item.SubjectRanks[subject].ObtainedMarks
				}
				pushItem := StudentObjItem{
					StudentID: item.StudentID,
					Marks:     Marks,
					index:     index + 1,
				}
				heap.Push(&pq, pushItem)
			}
			rank := 1
			studentSubjectWiseRanks[subject] = make(map[string]int)
			for pq.Len() > 0 {
				item := heap.Pop(&pq).(StudentObjItem)
				studentSubjectWiseRanks[subject][item.StudentID] = rank
				rank++
			}
		}
		// ordered data
		for _, item := range childNodesData {
			// make empty rank object
			subjectWiseMarks := make(map[string]StudentSubjectRank)
			for _, subject := range subjects {

				subjectWiseMarks[subject] = StudentSubjectRank{
					Rank:          studentSubjectWiseRanks[subject][item.StudentID],
					ObtainedMarks: item.SubjectRanks[subject].ObtainedMarks,
					Percentage:    item.SubjectRanks[subject].Percentage,
				}
			}
			studentRankObject := StudentRankAnalysis{
				StudentID:    item.StudentID,
				Marks:        item.Marks,
				Rank:         item.SubjectRanks["overall"].Rank,
				Hierarchy:    hierarchy,
				SubjectRanks: subjectWiseMarks,
			}
			orderedChildNodesData = append(orderedChildNodesData, studentRankObject)
		}
		hierarchyWiseRanks[hierarchy] = orderedChildNodesData
		data = orderedChildNodesData
	} else {
		mastreResultRecord, hasStudentData := studentMarksMap[hierarchy]
		if hasStudentData {
			totalObtainedMarks, totalMarks := 0, 0
			studentTopicAnalysis := mastreResultRecord.StudentTopicAnalysis
			subjectWiseMarks := make(map[string]StudentSubjectRank)
			// subject wise analysis
			for subject, data := range studentTopicAnalysis.Analysis {
				subjectWiseMarks[subject] = StudentSubjectRank{
					Rank:          1,
					ObtainedMarks: data.TotalObtainedMarks,
					Percentage:    float64(data.TotalObtainedMarks) / float64(data.TotalMarks) * 100.0,
				}
				totalObtainedMarks += data.TotalObtainedMarks
				totalMarks += data.TotalMarks
			}
			// overall data
			subjectWiseMarks["overall"] = StudentSubjectRank{
				Rank:          1,
				ObtainedMarks: totalObtainedMarks,
				Percentage:    float64(totalMarks) / float64(totalMarks) * 100.0,
			}
			// complete record
			studentData := StudentRankAnalysis{
				Rank:         1,
				StudentID:    hierarchy,
				Marks:        mastreResultRecord.StudentMarks,
				SubjectRanks: subjectWiseMarks,
			}
			data = append(data, studentData)
		}
	}
	return data
}

func calculateMarkAnalysis(
	hierarchy string,
	studentMarksMap map[string]*MasterResult, adjList map[string][]string, hierarchyMarkAnalysis map[string]HierarchyMarkAnalysis) {
	childNodes, hasChildNodes := adjList[hierarchy]
	if hasChildNodes {
		for _, childNode := range childNodes {
			calculateMarkAnalysis(childNode, studentMarksMap, adjList, hierarchyMarkAnalysis)
		}
		subjectWiseMarks := make(map[string]map[string]int)
		TestID := ""
		for _, childNode := range childNodes {
			childNodeData := hierarchyMarkAnalysis[childNode]
			TestID = childNodeData.TestID
			for subject, data := range childNodeData.MarkAnalysis {
				_, hasData := subjectWiseMarks[subject]
				if !hasData {
					subjectWiseMarks[subject] = make(map[string]int)
				}
				for index, value := range data {
					subjectWiseMarks[subject][index] += value
				}
			}
		}

		hierarchyMarkAnalysis[hierarchy] = HierarchyMarkAnalysis{
			TestID:       TestID,
			Hierarchy:    hierarchy,
			MarkAnalysis: subjectWiseMarks,
		}
	} else {
		mastreResultRecord, hasStudentData := studentMarksMap[hierarchy]
		if hasStudentData {
			studentTopicAnalysis := mastreResultRecord.StudentTopicAnalysis.Analysis
			subjectWiseMarks := make(map[string]map[string]int)
			for subject, item := range studentTopicAnalysis {
				subjectWiseMarks[subject] = make(map[string]int)
				strValue := strconv.Itoa(item.TotalObtainedMarks)
				subjectWiseMarks[subject][strValue]++
			}
			hierarchyMarkAnalysis[hierarchy] = HierarchyMarkAnalysis{
				TestID:       mastreResultRecord.TestID,
				Hierarchy:    hierarchy,
				MarkAnalysis: subjectWiseMarks,
			}
		}
	}
}

func calculateHierarchicalAnalysis(
	hierarchy string,
	studentMarksMap map[string]*MasterResult, adjList map[string][]string, hierarchyTopicAnalysis map[string]HierarchicalAnalysis) StudentTopicAnalysis {
	returnData := StudentTopicAnalysis{}
	childNodes, hasChildNodes := adjList[hierarchy]
	if hasChildNodes {
		kidsData := []StudentTopicAnalysis{}
		for _, childNode := range childNodes {
			kidData := calculateHierarchicalAnalysis(childNode, studentMarksMap, adjList, hierarchyTopicAnalysis)
			kidsData = append(kidsData, kidData)
		}
		hierarchyData := CalculateHierarchyAnalysis(kidsData)
		hierarchyTopicAnalysis[hierarchy] = HierarchicalAnalysis{
			StudentTopicAnalysis: hierarchyData,
			Hierarchy:            hierarchy,
		}
	} else {
		mastreResultRecord, hasStudentData := studentMarksMap[hierarchy]
		if hasStudentData {
			studentTopicAnalysis := mastreResultRecord.StudentTopicAnalysis
			studentData := []StudentTopicAnalysis{studentTopicAnalysis}
			returnData = CalculateHierarchyAnalysis(studentData)
		}
	}
	return returnData
}

func main() {
	// Needed for decoding null to static types
	rb := getCustomRegistryBuilder()
	clientOptions := options.Client().ApplyURI("mongodb://localhost:27017").SetRegistry(rb.Build())
	// Connect to MongoDB
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		log.Fatal(err)
	}
	// Check the connection
	err = client.Ping(context.TODO(), nil)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Connected to MongoDB!")
	start := time.Now()
	var wg sync.WaitGroup
	// adjList is parent to children adjency list for hierarchies
	adjList := make(map[string][]string)
	// hierarchyMap is childCode to hierarchy info for all hierarchies
	hierarchyMap := make(map[string]InstituteHierarchy)
	// studentsSnapshotMap is studentID to student's snapshot info map
	studentsSnapshotMap := make(map[string]StudentSnapshot)
	// questionMapping is question number to info map
	questionMapping := make(map[string]QuestionMapping)
	// step-1
	wg.Add(3)
	// fetch the preliminary information
	go getInstituteHierarchies(client, hierarchyMap, adjList, &wg)
	go getStudentsSnapshot(client, studentsSnapshotMap, &wg)
	go getQuestionMapping(client, questionMapping, &wg)
	wg.Wait()
	elapsed := time.Since(start)
	log.Println("Time taken to get pre required data from database", elapsed)

	// step-2
	stepTwo := time.Now()
	masterResultsMap := make(map[string]*MasterResult)
	studentHierarchyMap := make(map[string]*MasterResult)
	calculateMasterResults(adjList, studentsSnapshotMap, questionMapping, masterResultsMap, studentHierarchyMap)
	elapsed = time.Since(stepTwo)

	log.Println("Time taken to calculate master results", elapsed)
	// end of step-2

	// step-3
	stepThree := time.Now()
	hierarchyWiseRanks := make(map[string][]StudentRankAnalysis)
	calculateHierarchyRanks("Egni_u001_l11", masterResultsMap, adjList, hierarchyWiseRanks)
	elapsed = time.Since(stepThree)
	log.Println("Time taken to generate rank analysis", elapsed)
	// end of step-3

	// step-4
	stepFour := time.Now()
	hierarchyMarkAnalysis := make(map[string]HierarchyMarkAnalysis)
	hierarchyTopicAnalysis := make(map[string]HierarchicalAnalysis)
	calculateMarkAnalysis("Egni_u001_l11", masterResultsMap, adjList, hierarchyMarkAnalysis)
	calculateHierarchicalAnalysis("Egni_u001_l11", masterResultsMap, adjList, hierarchyTopicAnalysis)
	elapsed = time.Since(stepFour)
	log.Println("Time taken to generage hierarchy analysis", elapsed)
	// end of step-4

	// step-5
	// write masterresults
	stepFive := time.Now()
	var mrList []interface{}
	for _, item := range masterResultsMap {
		mrList = append(mrList, item)
	}
	mrCollection := client.Database(analysisDatabase).Collection("masterresults")
	mrCollection.InsertMany(context.TODO(), mrList)
	// write ranks data
	var rankList []interface{}
	for _, item := range hierarchyWiseRanks {
		for _, x := range item {
			rankList = append(rankList, x)
		}
	}
	rankAnalysisCollection := client.Database(analysisDatabase).Collection("rankAnalysis")
	rankAnalysisCollection.InsertMany(context.TODO(), rankList)
	// write hierarchical analysis
	var marksList []interface{}
	var hierarchyAnalysisList []interface{}
	for _, item := range hierarchyMarkAnalysis {
		marksList = append(marksList, item)
	}
	for _, item := range hierarchyTopicAnalysis {
		hierarchyAnalysisList = append(hierarchyAnalysisList, item)
	}
	marksListCollection := client.Database(analysisDatabase).Collection("markAnalysis")
	marksListCollection.InsertMany(context.TODO(), marksList)
	hierarchyAnalysisCollection := client.Database(analysisDatabase).Collection("hierarchicalAnalysis")
	hierarchyAnalysisCollection.InsertMany(context.TODO(), hierarchyAnalysisList)
	elapsed = time.Since(stepFive)
	log.Println("Time taken to write to db", elapsed)
	// end of step-5

	fmt.Println("done")
}
