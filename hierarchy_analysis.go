package main

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

// CalculateHierarchyAnalysis function for calculating hierarchical analysis
func CalculateHierarchyAnalysis(childrenData []StudentTopicAnalysis) StudentTopicAnalysis {
	//
	t := StudentTopicAnalysis{}
	t.Analysis = make(map[string]SubjectAnalysis)

	for _, childData := range childrenData {
		for subject, data := range childData.Analysis {
			subjectData, exists := t.Analysis[subject]
			if !exists {
				subjectData = SubjectAnalysis{}
				subjectData.Next = make(map[string]TopicAnalysis)
				subjectData.MinMarks = 1e9
				subjectData.MaxMarks = -1e9
			}
			subjectData.TotalMarks += data.TotalMarks
			subjectData.TotalObtainedMarks += data.TotalObtainedMarks
			subjectData.TotalQuestion += data.TotalQuestion
			subjectData.C += data.C
			subjectData.W += data.W
			subjectData.U += data.U
			subjectData.P += data.P
			subjectData.ADD += data.ADD
			subjectData.MinMarks = min(subjectData.MinMarks, data.MinMarks)
			subjectData.MaxMarks = max(subjectData.MaxMarks, data.MaxMarks)
			t.Analysis[subject] = subjectData
			for topic, topicData := range data.Next {
				prevTopicData, exists := t.Analysis[subject].Next[topic]
				if !exists {
					prevTopicData = TopicAnalysis{}
					prevTopicData.Next = make(map[string]SubTopicAnalysis)
					prevTopicData.MinMarks = 1e9
					prevTopicData.MaxMarks = -1e9
				}
				prevTopicData.TotalMarks += topicData.TotalMarks
				prevTopicData.TotalObtainedMarks += topicData.TotalObtainedMarks
				prevTopicData.TotalQuestion += topicData.TotalQuestion
				prevTopicData.C += topicData.C
				prevTopicData.W += topicData.W
				prevTopicData.U += topicData.U
				prevTopicData.P += topicData.P
				prevTopicData.ADD += topicData.ADD
				prevTopicData.MinMarks = min(prevTopicData.MinMarks, topicData.MinMarks)
				prevTopicData.MaxMarks = max(prevTopicData.MaxMarks, topicData.MaxMarks)
				t.Analysis[subject].Next[topic] = prevTopicData
				for subTopic, subTopicData := range topicData.Next {
					prevSubData, exists := t.Analysis[subject].Next[topic].Next[subTopic]
					if !exists {
						prevSubData = SubTopicAnalysis{}
						prevSubData.MinMarks = 1e9
						prevSubData.MaxMarks = -1e9
					}
					prevSubData.TotalMarks += subTopicData.TotalMarks
					prevSubData.TotalObtainedMarks += subTopicData.TotalObtainedMarks
					prevSubData.TotalQuestion += subTopicData.TotalQuestion
					prevSubData.C += subTopicData.C
					prevSubData.W += subTopicData.W
					prevSubData.U += subTopicData.U
					prevSubData.P += subTopicData.P
					prevSubData.ADD += subTopicData.ADD
					prevSubData.MinMarks = min(prevSubData.MinMarks, subTopicData.MinMarks)
					prevSubData.MaxMarks = max(prevSubData.MaxMarks, subTopicData.MaxMarks)
					t.Analysis[subject].Next[topic].Next[subTopic] = prevSubData
				}
			}
		}
	}
	return t
}
