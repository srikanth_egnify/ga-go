package main

// SubTopicAnalysis represents subTopicAnalysis in masterresults
type SubTopicAnalysis struct {
	CommonAnalysisStructure `bson:"inline"`
}

// TopicAnalysis represents topicAnalysis attr in masterresults
type TopicAnalysis struct {
	CommonAnalysisStructure `bson:"inline"`
	Next                    map[string]SubTopicAnalysis `bson:"next"`
}

// SubjectAnalysis represents subject analysis in masterresults
type SubjectAnalysis struct {
	CommonAnalysisStructure `bson:"inline"`
	Next                    map[string]TopicAnalysis `bson:"next"`
}

// StudentTopicAnalysis represent student topic analysis in masterresults
type StudentTopicAnalysis struct {
	Analysis map[string]SubjectAnalysis `bson:"topicAnalysis"`
}

// CommonCWUStructure is helper for cwuAnalysis in masterresults
type CommonCWUStructure struct {
	C     int `bson:"C"`
	W     int `bson:"W"`
	U     int `bson:"U"`
	P     int `bson:"P"`
	ADD   int `bson:"ADD"`
	UW    int `bson:"UW"`
	Marks struct {
		C   int `bson:"C"`
		W   int `bson:"W"`
		U   int `bson:"U"`
		P   int `bson:"P"`
		ADD int `bson:"ADD"`
		UW  int `bson:"UW"`
	} `bson:"marks"`
}

// CWUAnalysis represents student CWUAnalysis in masterresults
type CWUAnalysis struct {
	StudentCWUAnalysis map[string]CommonCWUStructure `bson:"cwuAnalysis"`
}

// StudentQuestionResponse represents student's response in masterresults
type StudentQuestionResponse struct {
	QuestionNumber string `bson:"questionNumber"`
	Subject        string `bson:"subject"`
	Topic          string `bson:"topic"`
	SubTopic       string `bson:"subTopic"`
	Response       string `bson:"response"`
}

// MasterResult represents record from masterresults
type MasterResult struct {
	StudentTopicAnalysis `bson:"inline"`
	CWUAnalysis          `bson:"inline"`
	TestID               string `bson:"testId"`
	StudentID            string `bson:"studentId"`
	ID                   string `bson:"_id"`
	NumberOfTests        int    `bson:"numberOfTests"`
	StudentMarks         int
	ResponseData         struct {
		QuestionResponse []StudentQuestionResponse `bson:"questionResponse"`
	}
}

// PopulateQuestionResponse populates the question response and initiates
// Topic Ananlysis and Sub Topic Analysis Map
func (mr *MasterResult) PopulateQuestionResponse(
	questionResponse map[string]string,
	questionMapping map[string]QuestionMapping) {
	for questionNumber, response := range questionResponse {
		qMap := questionMapping[questionNumber]
		if qMap.Topic == "" || qMap.SubTopic == "" {
			qMap.Topic = "Unmapped"
			qMap.SubTopic = "Unmapped"
		}
		subject, topic, subTopic := qMap.Subject, qMap.Topic, qMap.SubTopic
		studentQuestionResponse := StudentQuestionResponse{}
		studentQuestionResponse.QuestionNumber = questionNumber
		studentQuestionResponse.Response = response
		studentQuestionResponse.Subject = subject
		studentQuestionResponse.Topic = topic
		studentQuestionResponse.SubTopic = subTopic
		mr.ResponseData.QuestionResponse = append(mr.ResponseData.QuestionResponse, studentQuestionResponse)
		// other information

		subjectNext, exists := mr.Analysis[subject]
		if !exists {
			subjectNext = SubjectAnalysis{}
			subjectNext.Next = make(map[string]TopicAnalysis)
		}
		mr.Analysis[subject] = subjectNext

		topicNext, exists := mr.Analysis[subject].Next[topic]
		if !exists {
			topicNext = TopicAnalysis{}
			topicNext.Next = make(map[string]SubTopicAnalysis)
		}
		mr.Analysis[subject].Next[topic] = topicNext

		subTopicsNext, exists := mr.Analysis[subject].Next[topic].Next[subTopic]
		if !exists {
			subTopicsNext = SubTopicAnalysis{}
		}
		mr.Analysis[subject].Next[topic].Next[subTopic] = subTopicsNext
	}
}

// PopulateAnalysis populates the analysis data for master results
func (mr *MasterResult) PopulateAnalysis(
	questionResponse map[string]string,
	questionMapping map[string]QuestionMapping) {
	for questionNumber, response := range questionResponse {
		qMap := questionMapping[questionNumber]
		if qMap.Topic == "" || qMap.SubTopic == "" {
			qMap.Topic = "Unmapped"
			qMap.SubTopic = "Unmapped"
		}
		subject, topic, subTopic := qMap.Subject, qMap.Topic, qMap.SubTopic
		// subject level analysis
		topicAnalysisSubjectData := mr.Analysis[subject]
		// topic level analysis
		topicAnalysisTopicData := mr.Analysis[subject].Next[topic]
		// sub topic level analysis
		topicAnalysisSubTopicData := mr.Analysis[subject].Next[topic].Next[subTopic]

		topicAnalysisSubjectData.TotalMarks += qMap.C
		topicAnalysisTopicData.TotalMarks += qMap.C
		topicAnalysisSubTopicData.TotalMarks += qMap.C

		if response == "C" {
			topicAnalysisSubjectData.C++
			topicAnalysisSubjectData.TotalObtainedMarks += qMap.C

			topicAnalysisTopicData.C++
			topicAnalysisTopicData.TotalObtainedMarks += qMap.C

			topicAnalysisSubTopicData.C++
			topicAnalysisSubTopicData.TotalObtainedMarks += qMap.C
		} else if response == "W" {
			topicAnalysisSubjectData.W++
			topicAnalysisSubjectData.TotalObtainedMarks += qMap.W

			topicAnalysisTopicData.W++
			topicAnalysisTopicData.TotalObtainedMarks += qMap.W

			topicAnalysisSubTopicData.W++
			topicAnalysisSubTopicData.TotalObtainedMarks += qMap.W

		} else if response == "U" {
			topicAnalysisSubjectData.U++
			topicAnalysisSubjectData.TotalObtainedMarks += qMap.U

			topicAnalysisTopicData.U++
			topicAnalysisTopicData.TotalObtainedMarks += qMap.U

			topicAnalysisSubTopicData.U++
			topicAnalysisSubTopicData.TotalObtainedMarks += qMap.U

		} else if response == "ADD" {
			topicAnalysisSubjectData.ADD++
			topicAnalysisSubjectData.TotalObtainedMarks += qMap.ADD

			topicAnalysisTopicData.ADD++
			topicAnalysisTopicData.TotalObtainedMarks += qMap.ADD

			topicAnalysisSubTopicData.ADD++
			topicAnalysisSubTopicData.TotalObtainedMarks += qMap.ADD

		} else if response == "P" {
			topicAnalysisSubjectData.P++
			topicAnalysisSubjectData.TotalObtainedMarks += qMap.P

			topicAnalysisTopicData.P++
			topicAnalysisTopicData.TotalObtainedMarks += qMap.P

			topicAnalysisSubTopicData.P++
			topicAnalysisSubTopicData.TotalObtainedMarks += qMap.P
		}
		mr.Analysis[subject] = topicAnalysisSubjectData
		mr.Analysis[subject].Next[topic] = topicAnalysisTopicData
		mr.Analysis[subject].Next[topic].Next[subTopic] = topicAnalysisSubTopicData
	}
}

// PopulateCWUData populates the CWU data for student
func (mr *MasterResult) PopulateCWUData(
	questionResponse map[string]string,
	questionMapping map[string]QuestionMapping) {
	totalMarks := 0
	for questionNumber, response := range questionResponse {
		qMap := questionMapping[questionNumber]
		if qMap.Topic == "" || qMap.SubTopic == "" {
			qMap.Topic = "Unmapped"
			qMap.SubTopic = "Unmapped"
		}
		subject := qMap.Subject
		subjectCWUData := mr.StudentCWUAnalysis[subject]
		if response == "C" {
			subjectCWUData.C++
			subjectCWUData.Marks.C += qMap.C
			totalMarks += qMap.C
		} else if response == "W" {
			subjectCWUData.W++
			subjectCWUData.Marks.W += qMap.W
			totalMarks += qMap.W
		} else if response == "U" {
			subjectCWUData.U++
			subjectCWUData.Marks.U += qMap.U
			totalMarks += qMap.U
		} else if response == "ADD" {
			subjectCWUData.ADD++
			subjectCWUData.Marks.ADD += qMap.ADD
			totalMarks += qMap.ADD
		} else if response == "P" {
			subjectCWUData.P++
			subjectCWUData.Marks.P += qMap.P
			totalMarks += qMap.P
		}
		mr.StudentCWUAnalysis[subject] = subjectCWUData
	}
	mr.StudentMarks = totalMarks
}
