package main

import "container/heap"

// StudentObjItem is something we manage in priority queue
type StudentObjItem struct {
	StudentID string
	Marks     int
	index     int
}

// A PriorityQueue implements heap.Interface and holds Items.
type PriorityQueue []StudentObjItem

func (pq PriorityQueue) Len() int { return len(pq) }

func (pq PriorityQueue) Less(i, j int) bool {
	// We want Pop to give us the highest, not lowest, priority so we use greater than here.
	return pq[i].Marks > pq[j].Marks
}

func (pq PriorityQueue) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
	pq[i].index = i
	pq[j].index = j
}

// Push inserts value into priority queue
func (pq *PriorityQueue) Push(x interface{}) {
	n := len(*pq)
	item := x.(StudentObjItem)
	item.index = n
	*pq = append(*pq, item)
}

// Pop removes value from priority queue and returns it
func (pq *PriorityQueue) Pop() interface{} {
	old := *pq
	n := len(old)
	item := old[n-1]
	old[n-1] = StudentObjItem{} // avoid memory leak
	item.index = -1             // for safety
	*pq = old[0 : n-1]
	return item
}

// update modifies the priority and value of an Item in the queue.
func (pq *PriorityQueue) update(item *StudentObjItem, StudentID string, Marks int) {
	item.StudentID = StudentID
	item.Marks = Marks
	heap.Fix(pq, item.index)
}
